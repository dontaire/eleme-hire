$(document).ready(function () {

  var interval,
      boxIdx = 0;
  boxIdx = carousel(boxIdx);
  interval = autoLoop(boxIdx);
  $('.indicators a').click(function () {
    boxIdx = carousel($(this).parent('li').index());
    window.clearInterval(interval);
    interval = autoLoop(boxIdx);
  });

  $('.shop').hoverIntent(function() {
    popover($(this));
  }, function() {
    $('.shop-out-box').hide().removeClass(function (index, classString) {
      return (classString.match (/(^|\s)place-\S+/g) || []).join(' ');
    });
    $(this).find('.shop-name').removeClass('hover');
  });

});

var autoLoop = function (boxIdx) {
  return window.setInterval(function () {
    boxIdx = carousel(boxIdx);
  }, 5000);
};

var carousel = function (boxIdx) {
  var itemHeight = 100;
  $('.list-box').animate({
    top: (boxIdx * itemHeight * -1) + 'px'
  }, 500);

  $('.indicators a').removeClass('active');
  $('.indicators li').eq(boxIdx).children().addClass('active');

  return boxIdx < 3 ? boxIdx + 1 : 0;
};

var popover = function (element) {
  var position = {
    left   : element.offset().left - $(window).scrollLeft(),
    top    : element.offset().top - $(window).scrollTop(),
    right  : $(window).width() - element.width() - element.offset().left + $(window).scrollLeft(),
    bottom : $(window).height() - element.height() - element.offset().top + $(window).scrollTop()
  };
  console.log(position);

  var className = 'place-';
  className += position.bottom < 200 ? 'top-' : 'bottom-';
  className += position.right < 200 ? 'left' : 'right';

  element.children('.shop-out-box').show().addClass(className);
  element.find('.shop-name').addClass('hover');
};
